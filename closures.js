function counterFactory() {
  return object ={
   counterValue : 0,
   inc : function()
  	{
    this.counterValue++;
    return this.counterValue;
  	},
   dec :function ()
  	{
    this .counterValue--;
    return this .counterValue;
  	}
}
}




function limitFunctionCallCount(callBack, n) {
  let invokeCount  =0;
  return function myfunction( )
  {      
    if(invokeCount >= n)
    {
      return null;
    }
    else{
      callBack(invokeCount);
      invokeCount = invokeCount + 1; 
      myfunction();
    }
  }
}

function callBack(count) {
  console.log(`iteration count ----> ${count}. `);
}


function cb(value) {
  return value * value ;
}

function cacheFunction(cb) {
 let cache ={} ;
 return function invokingCache(cb,argV)
 {
  for(const attribute in cache)
  { 
    if([argV] == attribute)
    {
      return cache[attribute];
    }
  }

      cache[argV]=  cb(argV);
      return cache[argV];
  } 
}
