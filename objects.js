const testobject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' }; // use this object to test your functions

let arrayOfKeys = [];


function keys(testobject) {
for(const key in testobject)
{
  arrayOfKeys.push(key);
}
return arrayOfKeys;
}

let arrayOfValues = [];


function values(testobject) {
for(const key in testobject)
{
  arrayOfValues.push(testobject[key]);
}
return arrayOfValues;
}

// Third function Implementation */

function callBack(key,value)
{
  if (typeof value === 'string') {
    return value + "changed" ;
  }
  else
  {
    return value + 5 ;
  }
}


function mapObject(testobject1, callBack) {
for(const key in testobject)
{
    testobject1[key] = callBack(key,testobject1[key]);
}
return testobject1 ;
} 

function pairs(testobject)
{
let arrayPair = [];
for(const key in testobject)
{
   arrayPair.push([key,testobject[key]]);
}
return arrayPair;
}



function invert(testobject) {
  let resultObject = {} ;
  for(const key in testobject) {
    let v = testobject[key] ;
    resultObject[v] = key ;
  }
  return resultObject ; 
}



  
function defaults(defValue,testobject) {
  for( let key in testobject) {
    if(testobject[key] === undefined) {
      testobject[key] = defValue ;
    }
  }
}